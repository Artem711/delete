// # PLUGINS IMPORTS
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit"
import { save, load } from "redux-localstorage-simple"

// # COMPONENTS IMPORTS
import AppReducer from "./app/app.reducer"
import UserReducer from "./user/user.reducer"
import SwapReducer from "./swap/swap.reducer"

////////////////////////////////////////////////////////////////////////////////
const PERSISTED_KEYS = ["UserReducer", "TransactionsReducer", "ListsReducer"]

const store = configureStore({
  reducer: {
    AppReducer,
    UserReducer,
    SwapReducer,
  },
  middleware: [
    ...getDefaultMiddleware({ thunk: false }),
    // save({ states: PERSISTED_KEYS }),
  ],
  // preloadedState: load({ states: PERSISTED_KEYS }),
})

export default store

export type IRootState = ReturnType<typeof store.getState>
export type IRootDispatch = typeof store.dispatch
