// # PLUGINS IMPORTS
import { IRootState, IRootDispatch } from "state"
import { useDispatch, useSelector } from "react-redux"
import { useCallback } from "react"

// # COMPONENTS IMPORTS

////////////////////////////////////////////////////////////////////////////////

export function useSwapState(): IRootState["SwapReducer"] {
  return useSelector<IRootState, IRootState["SwapReducer"]>(
    (state) => state.SwapReducer
  )
}
