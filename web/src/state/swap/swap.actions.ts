import { createAction } from "@reduxjs/toolkit"

export enum Field {
  INPUT = "INPUT",
  OUTPUT = "OUTPUT",
}

export const selectCurrency = createAction<{
  field: Field
  currencyId: string
}>("SwapReducer/selectCurrency")
export const switchCurrencies = createAction<void>(
  "SwapReducer/switchCurrencies"
)
export const typeInput = createAction<{ field: Field; value: string }>(
  "SwapReducer/typeInput"
)
export const replaceSwapState = createAction<{
  field: Field
  value: string
  inputCurrencyId?: string
  outputCurrencyId?: string
  recipient: string | null
}>("SwapReducer/replaceSwapState")
export const setRecipient = createAction<{ recipient: string | null }>(
  "SwapReducer/setRecipient"
)
