import { createReducer } from "@reduxjs/toolkit"
import {
  Field,
  replaceSwapState,
  selectCurrency,
  switchCurrencies,
} from "./swap.actions"

export interface IState {
  readonly independentField: Field
  readonly value: string
  readonly [Field.INPUT]: {
    readonly currencyId: string | undefined
  }
  readonly [Field.OUTPUT]: {
    readonly currencyId: string | undefined
  }
  // the typed recipient address or ENS name, or null if swap should go to sender
  readonly recipient: string | null
}

const initialState: IState = {
  independentField: Field.INPUT,
  value: "",
  [Field.INPUT]: {
    currencyId: "",
  },
  [Field.OUTPUT]: {
    currencyId: "",
  },
  recipient: null,
}

export default createReducer<IState>(initialState, (builder) =>
  builder
    .addCase(
      replaceSwapState,
      (
        _,
        {
          payload: {
            value,
            recipient,
            field,
            inputCurrencyId,
            outputCurrencyId,
          },
        }
      ) => ({
        [Field.INPUT]: {
          currencyId: inputCurrencyId,
        },
        [Field.OUTPUT]: {
          currencyId: outputCurrencyId,
        },
        independentField: field,
        value: value,
        recipient,
      })
    )
    .addCase(selectCurrency, (state, { payload: { currencyId, field } }) => {
      const otherField = field === Field.INPUT ? Field.OUTPUT : Field.INPUT
      if (currencyId === state[otherField].currencyId) {
        // the case where we have to swap the order
        return {
          ...state,
          independentField:
            state.independentField === Field.INPUT ? Field.OUTPUT : Field.INPUT,
          [field]: { currencyId: currencyId },
          [otherField]: { currencyId: state[field].currencyId },
        }
      } else {
        // the normal case
        return {
          ...state,
          [field]: { currencyId: currencyId },
        }
      }
    })
    .addCase(switchCurrencies, (state) => ({
      ...state,
      independentField:
        state.independentField === Field.INPUT ? Field.OUTPUT : Field.INPUT,
      [Field.INPUT]: { currencyId: state[Field.OUTPUT].currencyId },
      [Field.OUTPUT]: { currencyId: state[Field.INPUT].currencyId },
    }))
)
