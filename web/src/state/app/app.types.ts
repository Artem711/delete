import { TokenList } from "@uniswap/token-lists"

export type IPopupList = Array<{
  key: string
  show: boolean
  content: IPopupContent
  removeAfterMs: number | null
}>

export type IPopupContent =
  | {
      txn: {
        hash: string
        success: boolean
        summary?: string
      }
    }
  | {
      listUpdate: {
        listUrl: string
        oldList: TokenList
        newList: TokenList
        auto: boolean
      }
    }

export enum IAppPopup {
  WALLET,
  SETTINGS,
  SELF_CLAIM,
  ADDRESS_CLAIM,
  CLAIM_POPUP,
  MENU,
  DELEGATE,
  VOTE,
}
