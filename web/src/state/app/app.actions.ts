import { createAction } from "@reduxjs/toolkit"
import { IAppPopup, IPopupContent } from "./app.types"

const reducerName = "app"
export const setOpenModal = createAction<IAppPopup | null>(
  `${reducerName}/setOpenModal`
)
export const addPopup = createAction<{
  key?: string
  removeAfterMs?: number | null
  content: IPopupContent
}>(`${reducerName}/addPopup`)
export const removePopup = createAction<{ key: string }>(
  `${reducerName}/removePopup`
)
