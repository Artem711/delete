import { createReducer, nanoid } from "@reduxjs/toolkit"
import { IPopupList, IAppPopup } from "./app.types"
import { addPopup, removePopup, setOpenModal } from "./app.actions"

interface IState {
  readonly popupList: IPopupList
  readonly openModal: IAppPopup | null
}

const initialState: IState = {
  popupList: [],
  openModal: null,
}

export default createReducer(initialState, (builder) =>
  builder
    .addCase(setOpenModal, (state, action) => {
      state.openModal = action.payload
    })
    .addCase(
      addPopup,
      (state, { payload: { content, key, removeAfterMs = 15000 } }) => {
        state.popupList = (key
          ? state.popupList.filter((item) => item.key !== key)
          : state.popupList
        ).concat([
          {
            key: key || nanoid(),
            show: true,
            content,
            removeAfterMs,
          },
        ])
      }
    )
    .addCase(removePopup, (state, { payload: { key } }) => {
      state.popupList.forEach((item) => {
        if (item.key === key) {
          item.show = false
        }
      })
    })
)
