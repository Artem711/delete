import { useSelector, shallowEqual } from "react-redux"
import { IRootState } from "state"

export function useIsDarkMode(): boolean {
  interface IReturn {
    userDarkMode: boolean | null
    matchesDarkMode: boolean
  }

  const { userDarkMode, matchesDarkMode } = useSelector<IRootState, IReturn>(
    ({ UserReducer: { matchesDarkMode, userDarkMode } }) => ({
      userDarkMode,
      matchesDarkMode,
    }),
    shallowEqual
  )

  return userDarkMode === null ? matchesDarkMode : userDarkMode
}
