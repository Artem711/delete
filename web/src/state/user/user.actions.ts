import { createAction } from "@reduxjs/toolkit"
import { create } from "domain"

const reducerName = "user"
export const updateMatchesDarkMode = createAction<{
  matchesDarkMode: boolean
}>(`${reducerName}/updateMatchesDarkMode`)

export const updateUserDarkMode = createAction<{ userDarkMode: boolean }>(
  `${reducerName}/updateUserDarkMode`
)
