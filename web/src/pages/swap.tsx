// # PLUGINS IMPORTS
import { Field } from "../state/swap/swap.actions"

// # COMPONENTS IMPORTS
import { SwapPoolTabs } from "components/layout/navbar-tabs"
import { PageWrap } from "components/wrappers"

// # EXTRA IMPORTS
import { Wrapper } from "components/organisms/swap/swap.styles"
import { AutoColumn } from "components/organisms/shared/column.styles"
import CurrencyInputPanel from "components/molecules/currency-input-panel"
import { useSwapState } from "state/swap/swap.hooks"

////////////////////////////////////////////////////////////////////////////////

export default function SwapPage() {
  const { independentField, value, recipient } = useSwapState()

  const formattedAmounts = {
    [independentField]: value,
    [dependentField]: showWrap
      ? parsedAmounts[independentField]?.toExact() ?? ""
      : parsedAmounts[dependentField]?.toSignificant(6) ?? "",
  }

  return (
    <PageWrap>
      <SwapPoolTabs active={"/swap"} />
      <Wrapper>
        <AutoColumn justify={"space-between"}>
          <CurrencyInputPanel
            text={formattedAmounts[Field.OUTPUT]}
            setText={handleTypeOutput}
            label={
              independentField === Field.INPUT && !showWrap && trade
                ? "To (estimated)"
                : "To"
            }
            showMaxButton={false}
            currency={currencies[Field.OUTPUT]}
            setCurrency={handleOutputSelect}
            otherCurrency={currencies[Field.INPUT]}
            id="swap-currency-output"
          />
        </AutoColumn>
      </Wrapper>
    </PageWrap>
  )
}
