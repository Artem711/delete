// # PLUGINS IMPORTS
import Link from "next/link"

// # COMPONENTS IMPORTS

// # EXTRA IMPORTS
import { routes } from "shared/constants"

////////////////////////////////////////////////////////////////////////////////

export default function Home() {
  return (
    <div>
      <Link href={routes.swap}> Swap</Link>
    </div>
  )
}
