// # PLUGINS IMPORTS
import { StrictMode } from "react"
import { createWeb3ReactRoot, Web3ReactProvider } from "@web3-react/core"
import { Provider } from "react-redux"

// # COMPONENTS IMPORTS
import BlocklistWrap from "components/wrappers/block-list-wrap"
import ThemeProvider, {
  FixedGlobalStyle,
  ThemedGlobalStyle,
} from "shared/theme/theme"
import languages from "languages"
import store from "state/index"

// # EXTRA IMPORTS
import { NetworkContextName } from "shared/constants"
import { getLibrary } from "shared/utils"

////////////////////////////////////////////////////////////////////////////////

// const Web3ProviderNetwork = createWeb3ReactRoot(NetworkContextName)
function App(Props: any) {
  return (
    <StrictMode>
      <FixedGlobalStyle />
      {/* <Web3ReactProvider getLibrary={getLibrary}> */}
      {/* <Web3ProviderNetwork getLibrary={getLibrary}> */}
      <BlocklistWrap>
        <Provider store={store}>
          <ThemeProvider>
            <ThemedGlobalStyle />
            <Props.Component {...Props.pageProps} />
          </ThemeProvider>
        </Provider>
      </BlocklistWrap>
      {/* </Web3ProviderNetwork> */}
      {/* </Web3ReactProvider> */}
    </StrictMode>
  )
}

export default languages.appWithTranslation(App)
