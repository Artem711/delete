// # PLUGINS IMPORTS
import NextI18Next from "next-i18next"
import path from "path"

// # COMPONENTS IMPORTS

////////////////////////////////////////////////////////////////////////////////

const translator = new NextI18Next({
  browserLanguageDetection: true,
  serverLanguageDetection: true,
  cleanCode: true,
  otherLanguages: [
    "ru",
    "de",
    "ro",
    "vi",
    "zh-CN",
    "zh-TW",
    "es-US",
    "es-AR",
    "iw",
  ],
  fallbackLng: "en",
  defaultLanguage: "en",
  localePath: path.resolve("./public/static/locales"),
})

export const useTranslation = translator.useTranslation
export const i18n = translator.i18n
export default translator
