export const routes = {
  swap: "/swap",
  pool: "/pool",
}

export const NetworkContextName = "NETWORK"

export const BLOCKED_ADDRESSES = []
