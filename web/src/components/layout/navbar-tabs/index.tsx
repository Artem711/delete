// # PLUGINS IMPORTS
import React from "react"
import styled from "styled-components"
import Link from "next/link"
import { darken } from "polished"
import { useTranslation, i18n } from "languages"

// # COMPONENTS IMPORTS

// # EXTRA IMPORTS
import { routes } from "shared/constants"

////////////////////////////////////////////////////////////////////////////////

interface IProps {
  active: "/swap" | "/pool"
}

const activeClassName = "ACTIVE"
export function SwapPoolTabs(props: IProps) {
  const { t } = useTranslation()

  return (
    <Tabs>
      <button
        onClick={() =>
          i18n.changeLanguage(i18n.language === "en" ? "ru" : "en")
        }
      >
        dsadas
      </button>
      <StyledLink href={routes.swap} isActive={props.active === routes.swap}>
        {t(routes.swap.replace("/", ""))}
      </StyledLink>
      <StyledLink href={routes.pool} isActive={props.active === routes.pool}>
        {t(routes.pool.replace("/", ""))}
      </StyledLink>
    </Tabs>
  )
}

////////////////////////////

const Tabs = styled.div`
  ${({ theme }) => theme.flexRowNoWrap}
  align-items: center;
  border-radius: 3rem;
  justify-content: space-evenly;
`

const StyledLink = styled(Link).attrs({
  activeClassName,
})`
  ${({ theme }) => theme.flexRowNoWrap}
  align-items: center;
  justify-content: center;
  height: 3rem;
  border-radius: 3rem;
  outline: none;
  cursor: pointer;
  text-decoration: none;
  color: ${({ theme }) => theme.text3};
  font-size: 20px;

  ${(props: { isActive: boolean }) => {
    if (props.isActive) {
      return `
      border-radius: 12px;
    font-weight: 500;
    color: ${({ theme }: any) => theme.text1};
    `
    }
  }};

  :hover,
  :focus {
    color: ${({ theme }) => darken(0.1, theme.text1)};
  }
`
