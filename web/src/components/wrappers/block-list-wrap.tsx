// # PLUGINS IMPORTS

import { ReactNode, useMemo } from "react"
import { BLOCKED_ADDRESSES } from "shared/constants"
import { useActiveWeb3React } from "shared/hooks/web3"

// # COMPONENTS IMPORTS

// # EXTRA IMPORTS

////////////////////////////////////////////////////////////////////////////////

export default function BlocklistWrap(props: { children: ReactNode }) {
  const { account } = useActiveWeb3React()
  const blocked: boolean = useMemo(
    () => Boolean(account && BLOCKED_ADDRESSES.indexOf(account) !== -1),
    [account]
  )

  if (blocked) {
    return <div>Blocked address</div>
  }
  return <>{props.children}</>
}
