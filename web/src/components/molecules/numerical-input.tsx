// # PLUGINS IMPORTS
import { memo } from "react"
import { regex } from "shared/utils"
import styled from "styled-components"

// # COMPONENTS IMPORTS

// # EXTRA IMPORTS

////////////////////////////////////////////////////////////////////////////////

interface IProps {
  text: string | number
  setText: (input: string) => void
  error?: boolean
  fontSize?: string
  align?: "right" | "left"
}

const inputRegex = RegExp(`^\\d*(?:\\\\[.])?\\d*$`) // match escaped "." characters via in a non-capturing group
export default memo(function NumbericalInput(
  props: IProps &
    Omit<React.HTMLProps<HTMLInputElement>, "ref" | "onChange" | "as">
) {
  const enforcer = (nextUserInput: string) => {
    if (
      nextUserInput === "" ||
      inputRegex.test(regex.escapeRegExp(nextUserInput))
    ) {
      props.setText(nextUserInput)
    }
  }

  return (
    <StyledInput
      {...props}
      value={props.value}
      onChange={(event) => {
        // replace commas with periods, because uniswap exclusively uses period as the decimal separator
        enforcer(event.target.value.replace(/,/g, "."))
      }}
      // universal input options
      inputMode="decimal"
      title="Token Amount"
      autoComplete="off"
      autoCorrect="off"
      // text-specific options
      type="text"
      pattern="^[0-9]*[.,]?[0-9]*$"
      placeholder={props.placeholder || "0.0"}
      minLength={1}
      maxLength={79}
      spellCheck="false"
    />
  )
})

///////////////////////////////////////

const StyledInput = styled.input<{
  error?: boolean
  fontSize?: string
  align?: string
}>`
  color: ${({ error, theme }) => (error ? theme.red1 : theme.text1)};
  width: 0;
  position: relative;
  font-weight: 500;
  outline: none;
  border: none;
  flex: 1 1 auto;
  background-color: ${({ theme }) => theme.bg1};
  font-size: ${({ fontSize }) => fontSize ?? "24px"};
  text-align: ${({ align }) => align && align};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0px;
  -webkit-appearance: textfield;

  ::-webkit-search-decoration {
    -webkit-appearance: none;
  }

  [type="number"] {
    -moz-appearance: textfield;
  }

  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }

  ::placeholder {
    color: ${({ theme }) => theme.text4};
  }
`
