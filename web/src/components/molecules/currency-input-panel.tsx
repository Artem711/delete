// # PLUGINS IMPORTS
import { Currency, Pair } from "@uniswap/sdk"
import styled from "styled-components"

// # COMPONENTS IMPORTS
import NumbericalInput from "./numerical-input"

// # EXTRA IMPORTS

////////////////////////////////////////////////////////////////////////////////

interface IProps {
  id: string

  text: string
  setText: (value: string) => void

  currency?: Currency | null
  setCurrency?: (currency: Currency) => void
  disableCurrencySelect?: boolean

  onMax?: () => void
  showMaxButton: boolean

  label?: string
  pair?: Pair | null

  hideBalance?: boolean
  hideInput?: boolean

  otherCurrency?: Currency | null
  showCommonBases?: boolean
  customBalanceText?: string
}

export default function CurrencyInputPanel({
  disableCurrencySelect = false,
  hideBalance = false,
  hideInput = false,
  label = "Input",
  pair = null, // used for double token logo
  ...restProps
}: IProps) {
  return (
    <InputPanel>
      <Container hideInput={hideInput}>
        <InputRow
          style={hideInput ? { padding: "0", borderRadius: "8px" } : {}}
          selected={disableCurrencySelect}
        >
          {!hideInput && (
            <>
              <NumbericalInput
                className="token-amount-input"
                text={restProps.text}
                setText={restProps.setText}
              />
            </>
          )}
        </InputRow>
      </Container>
    </InputPanel>
  )
}

//////////////////////////////////

const InputPanel = styled.div<{ hideInput?: boolean }>``
const Container = styled.div<{ hideInput: boolean }>``

const InputRow = styled.div<{ selected: boolean }>``
